#coding:utf-8
# .title .upper .capitalize .lower
#methode 1
import pickle
class human:
    def __init__(self, nom, prenom):
        self.nom = nom
        self.prenom = prenom
           
    def aff(self):
        print("je m'appelle {}, {}".format(self.nom,self.prenom))

try:

    h1 = human("Bouky", "KING")
    h1.aff()
    fic = open("test.txt", "r")

    with open("test.data", "w") as maFile:
        for x in fic:
            maFile.write(str(x))
    fic.close()  
    with open("human.data", "wb") as Files:
        rec = pickle.Pickler(Files)
        rec.dump(h1)
    with open("human.data", "rb") as Files:
        get_rec = pickle.Unpickler(Files)
        human_1 = get_rec.load()
    
    human_1.aff()

except:
    print("null")
    
"""
inventaire = ["Me", "Too", "Love", "this"]
for vls in inventaire:
    print(vls)
print(inventaire[1:2])
print(inventaire[1:])
print(inventaire[:2])
inventaire.insert(2, "I")
inventaire[len(inventaire):]= ["that", "because"]
print(inventaire[:])
inventaire.remove("this") #leve une except valueError
print(inventaire[:])
inventaire[2:] = ["I", "Love", "That", "Awsome"]
print(inventaire[:])

inventaire.sort()
inventaire.reverse()
print(inventaire[:])
print(inventaire.count("I"))
print(" ".join(inventaire))

import copy
liste1 = ["port", "bateaux", "mer"]
#liste2 = liste1
# ne fait pas de copy mais juste l'instanciation de l'adresse ---> liste2 = liste1
liste2 = copy.deepcopy(liste1)

print("liste 1 :", liste1)
print("liste 2 :", liste2)

liste2.append("sable")
liste2.append("palmier")

print("liste 1 :", liste1)
print("liste 2 :", liste2)
# liste1 + liste2 ,,, liste1.extend(liste2), liste1.concat
liste1+=liste2
for num_objet, val_object in enumerate(liste1):
    print("l'element d'indice {} est --> {}".format(num_objet, val_object))

#--------------------------------------------------------------------------------------

ma_chaine = "       [         jePsuisPlePpresidentPduPMali      ]      "
print(ma_chaine)
ch1=ma_chaine.capitalize()
print(ch1)
ch2=ma_chaine.strip()
print(ch2)
ch3 =ch2.center(70,"*")
print(ch3)
#  .find .index .replace
print("\n", ma_chaine.find("President"))
try:
    print("\n", ma_chaine.index("Suis"))
except ValueError :
    print("valeur non retrouver".center(80, " "))

ma_chaine3 = ma_chaine.split("P",10)
print(ma_chaine3)
ma_chaine1 = ma_chaine.replace("P"," ",10)
print(ma_chaine1)

class Vehicules:
    def __init__(self, nom_vehicule, Vitesse):
        self.nom= nom_vehicule
        self.vitesse = Vitesse
    
    def roule(self):
        print("{} roule a {}km/h".format(self.nom, self.vitesse))
    _roule=classmethod(roule)
class fille
class Voiture(Vehicules):
    def __init__(self, nom_voiture, essence, puissance):
        Vehicules.__init__(self, nom_voiture, essence)
        self.puissance = puissance
class VoitureSport(Voiture):
    def __init__(self, nom_voiture, essence, cadrage):
        Voiture.__init__(self, nom_voiture, essence)

class Avion(Vehicules):
    def __init__(self, nom, essence, marchandise):
        Vehicules.__init__(self, nom, essence)
        self.marchandise= marchandise


v1 = Vehicules("F22 raptor", 240)
v2 = Vehicules("Toyota supra", 200)
v3 = Voiture("Toyota supra", 90, 420)
print(v3.nom, "roule a puissance de", v3.puissance, "CH")
v1.roule()
v3.roule()
av1 = Avion("F22 Raptor", 2400, "Missiles")
av1.roule()
"""

